const chai = require("chai");
const expect = chai.expect;
const request = require("request");
const app = require("../src/server");
require('dotenv').config()
const port = process.env.PORT;
const protocol = process.env.PROTOCOL;
const host = process.env.HOST;

let server;

/** async request */
const arequest = async (value) => new Promise((resolve, reject) => {
    request(value, (error, response) => {
        if(error) reject(error)
        else resolve(response)
    });
});

describe("Test REST API", () => {
    beforeEach("Start server", async () => {
        server=app.listen(port);
    });
    describe("Test functionality", () => {
        it("POST /add body: {a:1, b:2} returns 3", async () => {
            const options = {
                method: 'POST',
                url: `${protocol}://${host}:${port}/add`,
                headers: {'content-type': 'application/json'},
                body: {a: 1, b: 2},
                json: true
            };
            await arequest(options).then((res) => {
                console.log({message: res.body})
                expect(res.body.sum).to.equal(3);
            }).catch((res) => {
                console.log({res});
                expect(true).to.equal(false, 'add function failed');
            })
        })
    });

    // Check if any test fails and close the server if so...
    afterEach(() => {
        server.close();
    });
});